#!/bin/bash

mkdir -p ./plots

./plot.py ./benchmarks/fft_128x1048576.csv threads time --plot=./plots/fft_128x1048576 --invert
./plot.py ./benchmarks/fft_1048576x128.csv threads time --plot=./plots/fft_1048576x128 --invert
./plot.py ./benchmarks/kmeans_mike.csv threads time --plot=./plots/kmeans_mike --invert

./plot.py ./benchmarks/parsec_ferret.csv threads real --plot=./plots/parsec_ferret --invert
./plot.py ./benchmarks/parsec_freqmine.csv threads real --plot=./plots/parsec_freqmine --invert
./plot.py ./benchmarks/parsec_x264.csv threads real --plot=./plots/parsec_x264 --invert

./plot.py ./benchmarks/splash2x_lu_cb.csv threads real --plot=./plots/splash2x_lu_cb --invert
./plot.py ./benchmarks/splash2x_ocean_cp.csv threads real --plot=./plots/splash2x_ocean_cp --invert
./plot.py ./benchmarks/splash2x_radiosity.csv threads real --plot=./plots/splash2x_radiosity --invert
