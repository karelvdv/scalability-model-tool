# Scalability Model Tool

Accompanies the paper "Characterizing the scalability of programs in many-core systems" by Karel van der Veldt, Cees de Laat, and Paola Grosso (not yet published).

## Documentation

### model.py
Defines the model and the minimization method to find the parameters given an input data set.

### plot.py
Plots a dataset and all four models fitted to it. You probably want to use this script.

### learn.py
Plotting with more options, but the code messy.