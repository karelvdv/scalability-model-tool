#!/usr/bin/env python
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
import model

def savefig(fig, file, format='png', dpi=None, transparent=False):
	"""Saves a figure to a file."""
	canvas = FigureCanvasAgg(fig)
	canvas.print_figure('%s.%s' % (file, format),
		format=format,
		dpi=dpi,
		transparent=transparent)

def sem(group, column):
	"""Standard error of the mean."""
	stdev = np.array(group.std()[column])
	count = np.array(group.size())
	return stdev / np.sqrt(count)

def normalize(ys, pivot_idx=0):
	"""Normalizes an array around a pivot point."""
	yp = ys[pivot_idx]
	return (ys / yp), yp

def invert(a, pivot_idx=0):
	"""Inverts an array around a pivot point."""
	return np.float(a[pivot_idx]) / a

def array_find_index(a, value):
	"""Finds the index of value in the array."""
	return np.nonzero(a == value)[0][0]

class DataSet:
	def __init__(self, df, x_label, y_label, invert_y=False, pivot=1):
		group = df.groupby(x_label)
		mean = group.mean()
		xs = np.array(mean.index).astype(int)
		ys = np.array(mean[y_label])
		ys_sem = sem(group, y_label) * 1.96
		ys_min = np.array(ys - ys_sem)
		ys_max = np.array(ys + ys_sem)
		if invert_y:
			ys = invert(ys)
			ys_min = invert(ys_min)
			ys_max = invert(ys_max)
		self.xs = xs
		pivot_idx = array_find_index(xs, pivot)
		self.ys, self.ys_pivot = normalize(ys, pivot_idx)
		self.ys_min, _ = normalize(ys_min, pivot_idx)
		self.ys_max, _ = normalize(ys_max, pivot_idx)
		self.pivot_idx = pivot_idx
	def plot(self, **line_args):
		plt.errorbar(self.xs, self.ys, yerr=self.ys - self.ys_min, **line_args)
		plt.plot(self.xs, self.ys, **line_args)

class ScalabilityCurve:
	def __init__(self, dataset, b_bounds, c_bounds, pivot=1):
		xs = dataset.xs
		ys = dataset.ys
		res = model.find_parameters(xs, ys,
			b_bounds=b_bounds,
			c_bounds=c_bounds,
			pivot_idx=array_find_index(xs, pivot),
		)
		self.params = tuple(res.x)
		self.pivot = pivot
	def fit(self, xs):
		pivot_idx = array_find_index(xs, self.pivot)
		ys = model.speedup(xs, *self.params)
		ys, _ = normalize(ys, pivot_idx)
		return ys
	def rsquared(self, dataset):
		return model.rsquared(dataset.ys, self.fit(dataset.xs))
	def plot(self, xs, **line_args):
		ys = self.fit(xs)
		plt.plot(xs, ys, **line_args)

def parseargs(argv):
	from argparse import ArgumentParser
	p = ArgumentParser()
	p.add_argument('--plot', default=None)
	p.add_argument('--invert', default=False, action='store_true')
	p.add_argument('--legendloc', default='lower right')
	p.add_argument('file')
	p.add_argument('x')
	p.add_argument('y')
	return p.parse_args(argv[1:])

def main(argv):
	args = parseargs(argv)

	df = pd.io.parsers.read_csv(args.file, comment='#')
	df = df.dropna()

	pivot = df[args.x][0]

	ds = DataSet(df, args.x, args.y, invert_y=args.invert, pivot=pivot)

	S_p = ScalabilityCurve(ds, b_bounds=(0, 1), c_bounds=(0, 1), pivot=pivot)
	S_u = ScalabilityCurve(ds, b_bounds=(0, 1), c_bounds=(0, 0), pivot=pivot)
	S_a = ScalabilityCurve(ds, b_bounds=(0, 0), c_bounds=(0, 0), pivot=pivot)
	S_g = ScalabilityCurve(ds, b_bounds=(0, 0), c_bounds=(1, 1), pivot=pivot)

	def print_params(name, s):
		print '%s (%f): a=%f, b=%f, c=%f' % ( (name, s.rsquared(ds)) + s.params )

	print_params('S_p', S_p)
	print_params('S_u', S_u)
	print_params('S_a', S_a)
	print_params('S_g', S_g)

	if args.plot:
		fig = plt.figure( figsize=(8, 6) )
#		plt.grid(True, 'both')
		plt.xlabel('Parallelism ($n$)', fontsize=16)
		plt.ylabel('Speedup', fontsize=16)

		xs = np.arange(1, ds.xs[-1] * 1.25)
		ds.plot(color='black', linewidth=1)
		S_p.plot(xs, linewidth=2, linestyle='-',  label='S_p (%.2f)' % S_p.rsquared(ds))
		S_u.plot(xs, linewidth=2, linestyle='--', label='USL (%.2f)' % S_u.rsquared(ds))
		S_a.plot(xs, linewidth=2, linestyle='-.', label='Amdahl (%.2f)' % S_a.rsquared(ds))
		S_g.plot(xs, linewidth=2, linestyle=':',  label='Gustafson (%.2f)' % S_g.rsquared(ds))

		axes = plt.gca()
		axes.set_ylim([0, None])
		axes.set_xlim([0, xs[-1]])

		legend = plt.legend(
			title='Models and R^2',
			loc=args.legendloc,
			prop={
				'size': 14, # font size
			}
		)
		plt.setp(legend.get_title(),fontsize=14)

		if args.plot == 'show':
			plt.show()
		else:
			savefig(fig, args.plot, format='eps')

	return 0

if __name__ == '__main__':
	sys.exit(main(sys.argv))