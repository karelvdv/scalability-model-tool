import numpy as np
from scipy.optimize import minimize

def speedup(n, a, b, c):
	x = 1. + a * c * (n - 1)
	y = (1. - a) + (1. + c * (n - 1)) * (a / n) + b * (n - 1)
	return x / y

def sumsqr(a, b):
	return np.sum( (a - b)**2 )

def rsquared(y_observed, y_predicted):
	"""R^2 is the fraction by which the variance of the errors is less than
	the variance of the dependent variable."""
	mean   = np.mean(y_observed)
	ss_tot = sumsqr(y_observed, mean)
	ss_res = sumsqr(y_observed, y_predicted)
	return 1.0 - (ss_res / ss_tot)

def rsquared_adj(y_observed, y_predicted, p):
	r2 = rsquared(y_observed, y_predicted)
	n  = len(y_predicted)
	return r2 - p * ((1. - r2) / (n - p - 1.))

def stdev_explained(r2):
	"""Percent of standard deviation explained by R^2."""
	return 1. - np.sqrt(1. - r2)

def find_parameters(xs, ys,
		pivot_idx=0,
		a_bounds=(0, 1),
		b_bounds=(0, 1),
		c_bounds=(0, 1)
	):
	def func_to_minimize(x, *a):
		z = speedup(xs, *x)
		return sumsqr(ys, z / z[pivot_idx])
	return minimize(
		fun=func_to_minimize,
		x0=np.zeros(3),
		method='L-BFGS-B',
		bounds=[
			a_bounds,
			b_bounds,
			c_bounds,
		]
	)

