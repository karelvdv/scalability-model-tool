#!/usr/bin/env python
import os
import numpy as np
import pandas as pd
from sys import stdout
import model

#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg

def get_cmap(n):
	import matplotlib.cm as cmx
	import matplotlib.colors as colors
	color_norm  = colors.Normalize(vmin=0, vmax=n - 1)
	scalar_map = cmx.ScalarMappable(norm=color_norm, cmap='hsv') 
	def map_index_to_rgb_color(index):
		return scalar_map.to_rgba(index)
	return map_index_to_rgb_color

def savefig(fig, file, format='png', dpi=None, transparent=False):
	canvas = FigureCanvasAgg(fig)
	canvas.print_figure('%s.%s' % (file, format),
		format=format,
		dpi=dpi,
		transparent=transparent)

def normalize(ys, pivot=0):
	yp = ys[pivot]
	return (ys / yp), yp

# standard error of the mean.
def sem(group, column):
	stdev = np.array(group.std()[column])
	count = np.array(group.size())
	return stdev / np.sqrt(count)

# standard error of the regression.
def ser(a, b):
	return np.mean( np.abs(a - b) )

def get_datapoints(df, x_label, y_label, id=None, invert=False):
	if id:
		df = df[df['id'] == id]
	group  = df.groupby(x_label)
	mean   = group.mean()
	xs     = np.array(mean.index).astype(int)
	ys     = np.array(mean[y_label])
	#ys_err = np.array( group.std()[y_label] )
	#ys_err[np.isnan(ys_err)] = 1.0
	ys_sem = sem(group, y_label) * 1.96
	ys_min = np.array(ys - ys_sem)
	ys_max = np.array(ys + ys_sem)
	#ys_std = np.array(group.std()[y_label])
	#ys_std[np.isnan(ys_std)] = 0.0
	#ys_min = np.array(ys - ys_std)
	#ys_max = np.array(ys + ys_std)
	if invert:
		ys     = np.float(ys[0])     / ys
		#ys_err = np.float(ys_err[0]) / ys_err
		ys_min = np.float(ys_min[0]) / ys_min
		ys_max = np.float(ys_max[0]) / ys_max
	return xs, ys, ys_min, ys_max

def index_array(a, indices):
	b = reduce(np.logical_or, (a == i for i in indices) )
	return np.nonzero(b)[0]

def array_find_index(a, value):
	return index_array(a, [value])[0]

def cmd_find(args):
	df = pd.io.parsers.read_csv(args.file, comment='#')
	df = df.dropna()

	xs, ys, ys_min, ys_max = get_datapoints(df,
		x_label=args.x,
		y_label=args.y,
		id=args.id,
		invert=args.invert
	)

	bmax = args.bmax
	if bmax < 0.0: bmax = None

	cmax = args.cmax
	if cmax < 0.0: cmax = None
	cmin = args.cmin

	start = args.start
	end   = args.end or None
	step  = args.step
	pivot = start

	x_data = xs[start:end:step]
	ys, pivot_val = normalize(ys, pivot)
	#ys_error, _ = normalize(ys_error, pivot)

	#np.random.seed(1337)
	res_y = model.find_parameters(x_data, ys[start:end:step],
		b_bounds=(0, bmax),
		c_bounds=(cmin, cmax)
	)

	ys_pred    = model.speedup(xs, *res_y.x)
	ys_pred, _ = normalize(ys_pred, pivot)

	print 'datapoints:', x_data
	print res_y
	print xs
	print ys
	print ys_pred
	print 'R^2:', model.rsquared(ys, ys_pred)

	ser_ys = model.speedup(x_data, *res_y.x)
	ser_ys, _ = normalize(ser_ys, pivot)
	print 'Close Fit R^2:', model.rsquared(ys[start:end:step], ser_ys)

	fig = plt.figure( figsize=(8, 6) )
	plt.grid(True, 'both')
	plt.xlabel('Parallelism ($n$)')
	plt.ylabel('Speedup')

	if args.plot:
		# plot observed data points.
		plt.fill_between(xs, ys_min, ys_max, alpha=0.10, color='black')
		plt.plot(xs, ys * pivot_val, color='black')

		# plot prediction.
		xs_plot = np.arange(1, xs[-1] * 1.5)
		pividx = array_find_index(xs_plot, xs[pivot])

		if args.std:
			ys_min, ys_min_pivot = normalize(ys_min, pividx)
			res_ymin = model.find_parameters(x_data, ys_min[start:end:step],
				b_bounds=(0, bmax),
				c_bounds=(0, cmax)
			)
			ys_max, ys_max_pivot = normalize(ys_max, pividx)
			res_ymax = model.find_parameters(x_data, ys_max[start:end:step],
				b_bounds=(0, bmax),
				c_bounds=(0, cmax)
			)
			ys_pred_min    = model.speedup(xs_plot, *res_ymin.x)
			ys_pred_min, _ = normalize(ys_pred_min, pividx)
			ys_pred_min   *= ys_min_pivot
			ys_pred_max    = model.speedup(xs_plot, *res_ymax.x)
			ys_pred_max, _ = normalize(ys_pred_max, pivot)
			ys_pred_max   *= ys_max_pivot
			plt.fill_between(xs_plot, ys_pred_min, ys_pred_max, alpha=0.10, color='blue')

		ys_plot    = model.speedup(xs_plot, *res_y.x)
		ys_plot, _ = normalize(ys_plot, pividx)
		ys_plot   *= pivot_val
		plt.plot(xs_plot, ys_plot, color='blue')
		#plt.plot(xs, ys_pred * pivot_val,color='blue')
		# draw markers
		y_marker = ys[start:end:step] * pivot_val #ys[x_data - 1] * pivot_val
		plt.plot(x_data, y_marker, color='red', marker='o', markersize=4.0, linestyle='None')
		plt.show()

		#figfile = os.path.split(args.file)[-1]
		#figfile = os.path.splitext(figfile)[0]
		#savefig(fig, './plots/' + figfile, format='png')
		savefig(fig, 'graph', format='png')



def get_cmap(n):
	import matplotlib.cm as cmx
	import matplotlib.colors as colors
	color_norm  = colors.Normalize(vmin=0, vmax=n - 1)
	scalar_map = cmx.ScalarMappable(norm=color_norm, cmap='hsv') 
	def map_index_to_rgb_color(index):
		return scalar_map.to_rgba(index)
	return map_index_to_rgb_color




def cmd_random(args):
	df = pd.io.parsers.read_csv(args.file, comment='#')
	df = df.dropna()

	xs, ys, ys_min, ys_max = get_datapoints(df,
		x_label=args.x,
		y_label=args.y,
		id=args.id,
		invert=args.invert
	)

	def get_result(x_data, y_data, x_pred, pividx):
		res = model.find_parameters(x_data, y_data,
			b_bounds=(0, args.bmax),
			c_bounds=(0, args.cmax)
		)
		y_pred    = model.speedup(x_pred, *res.x)
		y_pred, _ = normalize(y_pred, pividx)
		return res, y_pred

	if args.seed:
		np.random.seed(args.seed)

	xs_shuffle = np.copy(xs[args.start : args.end or None])
	np.random.shuffle(xs_shuffle)

	#fig = plt.figure( figsize=(8, 6) )

	found_models = []

	# calc the r2 when considering all points.
	best_res, best_ypred = get_result(xs, ys / ys[0], xs, 0)
	best_r2 = model.rsquared(ys, best_ypred * ys[0])
	print 'Best R^2:', best_r2

	for i in xrange(2, len(xs_shuffle) + 1, args.step):
		x_data = np.sort(xs_shuffle[:i])
		pividx = array_find_index(xs, x_data[0])

		y_norm, pivot_val = normalize(ys, pividx)
		y_data = y_norm[index_array(xs, x_data)]

		res, y_pred = get_result(x_data, y_data, xs, pividx)
		r2          = model.rsquared(y_norm, y_pred)
		print '%3d | R^2=%.3f (%.2f%%)' % (i, r2, 100.0 * (r2 / best_r2)), res.x

		found_models.append( (res, r2, x_data, pivot_val) )

		if args.plot:
			pivot = ys[pividx]
			plt.clf()

#			plt.grid(True, 'both')
			plt.xlabel(args.xlabel)
			plt.ylabel(args.ylabel)
			plt.title(args.title)

			# plot observed points
#			plt.fill_between(xs, ys_min, ys_max, alpha=0.10, color='black')
			plt.errorbar(xs, ys, yerr=ys - ys_min, color='black')
			plt.plot(xs, ys, color='black') #y_norm * pivot

			# plot sem
			if args.std:
				ymin_norm, ymin_pivot = normalize(ys_min, pividx)
				ymin_data = ymin_norm[x_data - 1]

				ymax_norm, ymax_pivot = normalize(ys_max, pividx)
				ymax_data = ymax_norm[x_data - 1]

				res_min, ymin_pred = get_result(x_data, ymin_data, xs, pividx)
				res_max, ymax_pred = get_result(x_data, ymax_data, xs, pividx)

				plt.fill_between(xs, ymin_pred * ymin_pivot, ymax_pred * ymax_pivot,
					alpha=0.10, color='blue')

			cmap = get_cmap(len(found_models))

			# plot model
			for model_num, (model_res, model_r2, model_x_data, model_ymult) in\
					enumerate(found_models):
				label = '%d pts, R2=%.2f' % (len(model_x_data), model_r2)
				color = cmap(model_num)
				#plt.plot(xs, y_pred * pivot, color='blue')
				x_plot = np.arange(1, xs[-1] + 1)
				y_plot = model.speedup(x_plot, *model_res.x)
				pividx_plot = array_find_index(x_plot, model_x_data[0])
				y_plot, _ = normalize(y_plot, pividx_plot)
				y_plot *= model_ymult
				plt.plot(x_plot, y_plot, color=color, label=label)

				# draw markers
	#			x_marker = x_data
	#			y_marker = y_data * pivot
				x_marker = 1 + index_array(x_plot, model_x_data)
				y_marker = y_plot[x_marker - 1] #* pivot
				plt.plot(x_marker, y_marker, color=color, marker='o', markersize=5.0,
					linestyle='None')

			axes = plt.gca()
			axes.set_ylim([1, args.ymax])

			# show plot
			plt.legend(
				loc='best',
				prop={
					'size': 9, # font size
				}
			)
			# save plot to file
			#plt.savefig('graph.png', format='png')
			plt.savefig('graph.eps', format='eps')
			plt.show()


def parse_args(args):
	from argparse import ArgumentParser
	p = ArgumentParser()
	subp = p.add_subparsers(dest='command')
	findp = subp.add_parser('find', help='find parameters')
	findp.add_argument('--sep', help='row separator', default=',')
	findp.add_argument('--start', type=int, default=0)
	findp.add_argument('--end', type=int, default=0)
	findp.add_argument('--step', type=int, default=1)
	findp.add_argument('--bmax', type=float, default=1)
	findp.add_argument('--cmax', type=float, default=1)
	findp.add_argument('--cmin', type=float, default=0)
	findp.add_argument('--csv', default=False, action='store_true')
	findp.add_argument('--noheader', default=False, action='store_true')
	findp.add_argument('--plot', default=False, action='store_true')
	findp.add_argument('--id', default=None)
	findp.add_argument('--std', default=False, action='store_true')
	findp.add_argument('--invert', default=False, action='store_true')
	findp.add_argument('--xlabel', default='')
	findp.add_argument('--ylabel', default='')
	findp.add_argument('--title', default='')
	findp.add_argument('x', help='x-axis column name')
	findp.add_argument('y', help='y-axis column name')
	findp.add_argument('file', help='file name')
	randp = subp.add_parser('rand', help='find parameters')
	randp.add_argument('--seed', type=int, default=0)
	randp.add_argument('--start', type=int, default=0)
	randp.add_argument('--end', type=int, default=0)
	randp.add_argument('--step', type=int, default=1)
	randp.add_argument('--bmax', type=float, default=1)
	randp.add_argument('--cmax', type=float, default=1)
	randp.add_argument('--id', default=None)
	randp.add_argument('--invert', default=False, action='store_true')
	randp.add_argument('--plot', default=False, action='store_true')
	randp.add_argument('--std', default=False, action='store_true')
	randp.add_argument('--xlabel', default='')
	randp.add_argument('--ylabel', default='')
	randp.add_argument('--title', default='')
	randp.add_argument('--ymax', default=None, type=int)
	randp.add_argument('x', help='x-axis column name')
	randp.add_argument('y', help='y-axis column name')
	randp.add_argument('file', help='file name')

	return p.parse_args(args)

def main(args):
	args = parse_args(args)

	if args.command == 'find':
		cmd_find(args)
	elif args.command == 'graph':
		cmd_graph(args)
	elif args.command == 'rand':
		try:
			cmd_random(args)
		except KeyboardInterrupt:
			pass

if __name__ == '__main__':
	import sys
	sys.exit( main(sys.argv[1:]) )
